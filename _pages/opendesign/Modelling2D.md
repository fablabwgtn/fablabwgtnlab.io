---
layout: page
title: "2D Modelling"
subheadline:  "vector based fun"
teaser: ""
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_inkscape.jpg
url: "/book/"
order: 1
tag: modelling
permalink: /opendesign/modelling-2d/
---
The first session is an introduction to fundamental concepts in vector-based design for digital fabrication. You’ll be given challenges to prototype basic forms and use a range of tools.
In the second session, you will be putting what you learnt in the last session into practice by creating a lovely sticker to cut out on the Vinyl cutter.

$TBC (materials incl.),
2 x 2HR sessions,
No pre-req
