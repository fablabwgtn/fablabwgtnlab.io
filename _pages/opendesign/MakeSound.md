---
layout: page
title: "Make your own Audio Reactive Board"
subheadline:  "LEDs, Noise, Movement"
teaser: "With Daniel Harmsworth. "
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_sounds.jpg
url: "/book/"
order: 1
tag: advanced
permalink: /opendesign/make-sound/
---
This series will teach you basic audio processing on a microcontroller that takes an audio input, analyses it, and outputs it to LED lights. You will also do some SMD soldering.

$TBC (materials incl.), 2 x 2HR sessions, Basic Electronics 01 & 04 req
