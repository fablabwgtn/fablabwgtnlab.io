---
layout: page
title: "Make your own Home monitoring System"
subheadline:  "Data Sovereignty and environmental management"
teaser: "With Daniel Harmsworth."
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_electronics2.jpg
url: "/book/"
order: 2
tag: advanced
permalink: /opendesign/make-moniter/
---
This series will give you the skills to take control of your home’s data systems. Design and make IOT devices to read heating and power consumption data, and turn lights on and off according to requirements.

$TBC (materials incl.), 4 x 2HR sessions, Electronics Essentials req
