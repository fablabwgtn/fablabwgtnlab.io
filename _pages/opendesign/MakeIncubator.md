---
layout: page
title: "Make your own Incubator"
subheadline:  "Growing goodness at home"
teaser: "With Wendy Neale and Omelia Iliffe. "
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_giyah.jpg
url: "/book/"
order: 1
tag: advanced
permalink: /opendesign/make-incubator/
---
This series will teach you how to make all the parts of an incubator. Cast food-safe silicon, laser-cut a container, program a circuitboard and make the heater and temperature sensor.

You need to complete all the _Electronics Essentials_ & the 2D modelling Essential courses before participating in this course.

Four Saturday sessions, $TBC
