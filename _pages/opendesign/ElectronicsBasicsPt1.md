---
layout: page
title: "Basic Electronics 01"
subheadline:  "Breadboarding and Paper Electronics"
teaser: "This is a good opportunity to brush up on Ohm’s Law and electronics puns."
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_electronics-1st.jpg
order: 1
tag: electronics
permalink: /opendesign/electronics-1/
---
The first session is an introduction to fundamental concepts in electronics design. You’ll be given challenges to prototype basic circuit components on breadboards. This is a good opportunity to brush up on Ohm’s Law and electronics puns. And if you don’t understand any of these things now, know that you will by the end of the session.
In the second session, you will be putting what you learnt in the last session into practice by creating a few items yourself with our Paper Electronics kit. You will also begin to create a USB powered LED lamp and have some homework for the next session.

$TBC (materials incl.),
2 x 2HR sessions,
No pre-req
