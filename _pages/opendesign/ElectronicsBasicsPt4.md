---
layout: page
title: "Basic Electronics 04"
subheadline:  "Make your own circuit board"
teaser: "mill and populate your RGB LED circuit board..."
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_anna-sold.jpg
url: "/book/"
order: 4
tag: electronics
permalink: /opendesign/electronics-4/
---
Learn how to mill, populate and program your own circuit board. This is one of the first Fab Academy boards and it has an RGB LED on it which you can program to cycle through colours (we’ll be using the Arduino IDE this time). You will be soldering very small (SMD) components, so make sure you don’t have too much caffeine beforehand.

$TBC (materials incl.),
3 x 2HR sessions,
Basic Electronics 01 and 03 req
