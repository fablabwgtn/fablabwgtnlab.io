---
layout: page
title: "Basic Electronics 02"
subheadline:  "Soft Materials & Interactions"
teaser: "Make your own button..."
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_electronics-soft.jpg
url: "/book/"
order: 2
tag: electronics
permalink: /opendesign/electronic-2/
---
One challenge for these sessions is to make your own button for your LED lamp – learning about soft materials for electronics – and the other challenge is to create your own vector pattern for the lamp, using Inkscape and a Vinyl cutter. Whichever challenge you choose for this session, there’s time in the next session for you to do the other.

$TBC (materials incl.),
2 x 2HR sessions,
Basic Electronics 01 req
