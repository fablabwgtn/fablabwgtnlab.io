---
layout: page
title: "Basic Electronics 03"
subheadline:  "Get into the Grove"
teaser: "Grove is a programmable system... "
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_electronics1.jpg
url: "/book/"
order: 3
tag: electronics
permalink: /opendesign/electronics-3/
---
Exploring the Grove kit, a programmable system that allows you to ‘plug & play’ with a range of input & output devices. Make lights flash when it’s too noisy, buzz when it’s shaken etc. Using a block-based system, it’s programming but without all the complicated stuff (that comes next…).

$TBC (kit NOT incl.),
3 x 2HR sessions,
Basic Electronics 01 req
