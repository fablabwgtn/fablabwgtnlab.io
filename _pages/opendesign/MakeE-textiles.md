---
layout: page
title: "Make your own E-textiles"
subheadline:  "Woven LED Matrix"
teaser: "With Bridget McKendry. "
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_etext.jpg
url: "/book/"
order: 3
tag: advanced
permalink: /opendesign/make-etextiles/
---

First session - using the kit provided, the participants will experiment with a paper electronics approach to prototyping their ideas for e-textiles.
Second session - using the kit provided, the participants will build on the previous session by creating a woven e-textile with embedded LEDs and control it via a digital interface.

Basic Electronics 01 req, 2 x 2 hours, $ TBC
