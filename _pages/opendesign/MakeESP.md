---
layout: page
title: "Prototype with our SewableESP"
subheadline:  "Powerful fun"
teaser: "With Omelia Iliffe. "
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_sewableESP.jpg
url: "/book/"
order: 4
tag: advanced
permalink: /opendesign/make-esp/
---
This series will teach you how to prototype with our Sewable ESP prototyping card.

You need to complete all the _Electronics Essentials_ courses before participating in this course.

$TBC, Two Saturday sessions, Electronic Essentials req
