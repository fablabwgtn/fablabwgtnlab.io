---
layout: page
title: "3D Modelling"
subheadline:  "Volumes and Spaces"
teaser: ""
categories:
    - events
show_meta: false
header:
   image_fullwidth: events_fusion.jpg
url: "/book/"
order: 4
tag: modelling
permalink: /opendesign/modelling-3d/
---
Working with one of the Autodesk offerings (inventor or Fusion 360), you'll learn the basics of planning and executing 3D design objects with a parametric focus. The objective for these sessions is to become comfortable within a modelling environment and develop a workflow that is suitable for 3D printing, CNC milling or Laser cutting.

$TBC, 3 x 2hr sessions
