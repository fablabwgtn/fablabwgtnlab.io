---
layout: page
header:
  image_fullwidth: homepage_hero.png
  position: left
  image_alt: Fab Lab Wgtn Logo Hero
subheadline: ""
#teaser: "Nurturing resilience through hands-on experimentation and lifelong learning in global open design ecosystems"
permalink: /index.html
---


We're all about **hands-on experimentation**, **learning for life**, an **open-source** ethos, going from **digital to physical** and back again, **DIY & DIWO** (do it with others), with a big emphasis on **workflow**.

We support **students & staff** at the **College of Creative Arts, Massey University**.  
All of our educational programming is designed to **empower you to make almost anything** - while instilling our core values of **inclusivity and sustainability** into your practice.

<ul class="nav nav-pills nav-fill mt-4" id="homepageTabs" role="tablist">
  <li class="nav-item m-1" role="presentation">
    <a class="nav-link active text-nowrap" id="massey-tab" data-toggle="tab" href="#massey" role="tab" aria-controls="massey" aria-selected="true">
      <h5 class="m-0">I am at <strong>Massey</strong></h5>
    </a>
  </li>
  <!-- <li class="nav-item m-1" role="presentation">
    <a class="nav-link text-nowrap" id="learn-tab" data-toggle="tab" href="#learn" role="tab" aria-controls="learn" aria-selected="true">
      <h5 class="m-0">I am <strong>curious</strong></h5>
    </a>
  </li> -->
  <li class="nav-item m-1" role="presentation">
    <a class="nav-link text-nowrap" id="youngmaker-tab" data-toggle="tab" href="#youngmaker" role="tab" aria-controls="youngmaker" aria-selected="false">
      <h5 class="m-0">I am a <strong>young maker</strong></h5>
    </a>
  </li>
  <li class="nav-item m-1" role="presentation">
    <a class="nav-link text-nowrap" id="somethingmade-tab" data-toggle="tab" href="#somethingmade" role="tab" aria-controls="somethingmade" aria-selected="false">
      <h5 class="m-0">I want to get <strong>something made</strong></h5>
      </a>
  </li>
</ul>

---

<div class="tab-content m-1 mt-3" id="homepageTabContent">
  <div class="tab-pane fade show active" id="massey" role="tabpanel" aria-labelledby="massey-tab">
    {% include list.html category='homepage' column_size=4 tag='massey' %}
  </div>
  <!-- <div class="tab-pane fade show" id="learn" role="tabpanel" aria-labelledby="learn-tab">
    {% include list.html category='homepage' tag='learn' %}
  </div> -->
  <div class="tab-pane fade" id="youngmaker" role="tabpanel" aria-labelledby="youngmaker-tab">
    {% include list.html category='homepage' tag='young' %}
  </div>
  <div class="tab-pane fade" id="somethingmade" role="tabpanel" aria-labelledby="somethingmade-tab">
    {% include list.html category='homepage' tag='justa' %}
  </div>
</div>
