---
layout: page
title:  "Open Design & Making"
teaser: "Join us to explore the possibilities of open design & digital fabrication."
categories:
    - homepage
header:
   image_fullwidth: homepage_hold.png
   image_preview: homepage_weeklyevents.jpg
show_meta: false
tag: learn
permalink: /opendesign/
---

**We are all managing major change and upheaval in what is becoming ‘Covid-normal’ Therefore this programme is on hold for the foreseeable future. We are also trying not to produce unnecessary noise, and have put a hold on our quarterly newsletter, facebook and instagram posts.**

Fab Lab Wgtn is where DIYers, makers, coders, designers and artists take their creative skills to the next level. You'll be in good company. We are an open-source community engaged in learning through making.

Our Open Design & Making Programme, which we host through <a href="https://www.massey.ac.nz/massey/explore/departments/pace/pace_home.cfm"> Massey's PaCE programme</a>, consists of these series at present:

## Electronic Essentials
The *Electronics Essentials* series is designed to springboard you into this creative space, as electronic skills are essential for almost any project created here.

{% include list.html category='events' tag='electronics' %}



## Modelling Essentials
The *Modelling Essentials* series is designed to springboard you into this creative space, as these skills are also essential for many projects created here.

{% include list.html category='events' tag='modelling' %}



## Project Based
The *Project* series is designed to embed the knowledge you gained previously and give you a broader range of skills and techniques.

{% include list.html category='events' tag='advanced' %}
