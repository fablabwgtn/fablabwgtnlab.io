---
layout: page
title:  "Pop Up"
teaser: "Host a Fab Lab Wgtn experience."
categories:
    - homepage
header:
   large: true
   image_fullwidth: homepage_popuplab_large.jpg
   image_preview: homepage_popuplab.jpg
show_meta: false
tag: learn
permalink: /popups/
---
We have a range of portable equipment that we install at an event or venue for a specific amount of time, making the tools and associated activities even more accessible. This equipment can include 3D Printers, vinyl cutters, screen printing equipment, electronic kits and DIYbio projects. We have had Pop Up Labs at conferences, at community venues/halls, Society meetings, other Campuses, offices and at Makertorium & MakerFaire and we are always interested in other opportunities. We develop specific programmes to meet the needs of different communities, drawing from a diverse range of expertise. Call us to discuss your next team building, professional development or strategic planning day.
