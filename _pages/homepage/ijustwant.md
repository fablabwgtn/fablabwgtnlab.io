---
layout: page
title:  "Digitally Creative Businesses"
teaser: "where to go"
categories:
    - homepage
header:
   image_fullwidth: homepage_otheroptions.jpg
   image_preview: homepage_otheroptions.jpg
show_meta: false
tag: justa
permalink: /about/otheroptions/
---
If you just want to have something made, or have a very tight deadline, there are  many online and local businesses that can create your idea for you.  
Here are a few that we can recommend:  

[Human Dynamo](https://www.humandynamo.co.nz/portfolios.html)

[Ponoko](https://www.ponoko.com)

[Shapeways](http://www.shapeways.com)

[Graleys](https://www.graleyplastics.co.nz/)

[Makerspace Johnsonville](https://www.wcl.govt.nz/services/technology/makerspace/)
