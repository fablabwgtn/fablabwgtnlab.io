---
layout: page
title:  "Fab Academy"
teaser: "Learning design thinking and agile development while engaging with all aspects of the Fab Lab ecosystem."
categories:
    - homepage
header:
   image_fullwidth: homepage_fabacademy.jpg
   image_preview: homepage_fabacademy.jpg
show_meta: false
tag: learn
permalink: /fabacademy/
---


#### Hosting Locally

We no longer host Fab Academy locally, due to time zone difficulties - it's not possible when classes are 3 hours long and are at 3.00am. You can see below that everyone was sort of perky the one time we did it, but they were very sleepy the next day.

{% include image.html src="about_fabacademy04.jpg" alt="Attending class at 2am" %}

#### Mentoring

We are able to mentor new labs who wish to participate in Fab Academy, meaning that our experience qualifies us to remotely supervise students in less experienced Labs in the Pacific region/nearby time-zones. If your local Fab Lab is looking for a mentor to work with, please contact [coordination@fabacademy.org](mailto:coordination@fabacademy.org)


#### Local Academy

We have developed a locally-focussed course that allows people to engage according to their own life and work schedules. If you're interested in our Open Design & Making course, please check out our page here.


#### Fab Academy

is a Digital Fabrication programme directed by Neil Gershenfeld of MIT’s Center For Bits and Atoms and based on MIT’s rapid prototyping course, MAS 863: How to Make (Almost) Anything.

#### Some of our Fab Academy Graduates

[Anna Aflalo](http://fabacademy.org/archives/2015/as/students/aflalo.anna/index.html)

[Ben Matthes](http://fabacademy.org/archives/2015/as/students/matthes.ben/index.html)

[Craig Hobern](http://fabacademy.org/archives/2015/as/students/hobern.craig/index.html)

[Daniel Harmsworth](http://fabacademy.org/archives/2015/as/students/harmsworth.daniel/index.html)

[Geoffrey Desborough](http://fabacademy.org/archives/2015/as/students/desborough.geoffrey/index.html)

[Jasmin Cheng](http://fabacademy.org/archives/2015/as/students/cheng.jasmin/index.html)

[Ki Fredeen](http://archive.fabacademy.org/archives/2016/fablabalaska/students/992/)
