---
layout: page
title:  "Under 18s"
## subheadline: ""
teaser: "spaces for making"
categories:
    - homepage
header:
   image_fullwidth: homepage_youngmakers.png
   image_preview: homepage_youngmakers.png
show_meta: false
tag: young
permalink: /about/youngmakers/
---


If you would like to do an undergraduate degree but don't have the academic requirements for University Entrance, you can apply to the Foundation studies Certificate and select the Creative Pathway. To find out more, please look at the [Yellow Book](https://www.massey.ac.nz/documents/276/toi-rauwharangi-course-guide.pdf) for 2025.

There are places around Wellington that offer interesting activities if you are under 18 and would like to learn about making things and electronics.  

Here are a few that we can recommend:  

[Capital E](https://www.capitale.org.nz/)

[MakeRoom](https://makeroomwellington.nz/)

[MakerBox](https://makerbox.wordpress.com/)
