---
layout: page
subheadline: "Credits"
teaser: ""
permalink: /credits/
header: no
---
Fab Lab Wgtn would like to thank all the awesome people and projects that made this site possible.

### Built by:  
Wendy Neale  
Jasmin Cheng  
Craig Hobern  
Omelia Iliffe  
Daniel Harmsworth

#### with support from:  
[Giorgia Negro](https://www.behance.net/giorgianegro)  
Rox & Alex from [xequals](http://www.xequals.co.nz)  

#### Featuring photos by:  
[Geoffrey Desborough](http://geoffreydesborough.com)  
Minjoo Bai  
Wendy Neale  
Jasmin Cheng  
Craig Hobern  
Anna Aflalo  
Zoilo Abad  
Shannon Birt  

#### using:  
[Icons by Daniel Bruce](http://entypo.com/)  
a striped back [Feeling responsive](https://github.com/Phlow/feeling-responsive) Theme by [phlow](http://phlow.de/)  
[Bootstrap](https://getbootstrap.com)  
[Jekyll](https://jekyllrb.com/community/)  
