---
layout: page
title: "Contact"
meta_title: "Contact Us"
subheadline: ""
teaser: ""
permalink: "/contact/"
header: no
sidebar:
  column_size: 6
  contains:
    map
---

Our postal address is:  
**Fab Lab Wgtn,  
Nga Pae Mahutonga | School of Design  
Massey University  
PO Box 756  
Wellington 6041  
Aotearoa | New Zealand**

Email: **[fablabwgtn@massey.ac.nz](mailto:fablabwgtn@massey.ac.nz)**  

##### Our Regular Opening Hours:

Monday - Friday, 8:00am - 4.00pm

