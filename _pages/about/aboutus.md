---
layout: page
title:  "About Us"
teaser: "Find out who the core staff are."
categories:
    - about
header:
   image_fullwidth: homepage_aboutus.jpg
   image_preview: homepage_aboutus.jpg
show_meta: false
tag: involved
permalink: "/about/whoweare/"
---


#### The core staff for 2025

##### Eva Ferguson Rebenschied
_she|her_

Eva is a Textile Design graduate who started in FabLab in 2022. She is passionate about pottery, knitting and the technical aspects of design.


##### Ryan Greer
_he|him_

Ryan joined the Fab Lab in 2024 after graduating with his Master of Design in 2023. He enjoys all things Industrial, lighting and photography.

##### Elizabeth Fountaine
_she|her_

Elizabeth is the 2025 Fab Lab Graduate Technician. She graduated in 2024 with an Integrated Design degree and will be learning the ropes this year.
