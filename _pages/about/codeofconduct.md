---
layout: page
title:  "Code of Conduct"
## subheadline:  "Creating a Safe Environment"
teaser: ""
categories:
    - about
header:
   image_fullwidth: homepage_hold.png
permalink: "/about/codeofconduct/"
---
## He waka eke noa | We are all in this together  

This Code of Conduct outlines our expectations of participants within the Fab Lab Wgtn ecosystem, as well as steps for reporting unacceptable behaviour. We are committed to creating a welcoming and inspiring ecosystem for all and our code of conduct is fundamental to this.

As part of creating an accessible and de-colonised space, we honour the significance of indigenous perspectives. Accordingly, we strive to create an environment where diverse approaches to knowing, communication styles, attitudes towards conflict, approaches to completing tasks, decision making styles and attitudes towards disclosure are acknowledged as enriching our culture.

Fab Lab Wgtn is committed to the ideals expressed in the [Fab Charter](http://fab.cba.mit.edu/about/charter/) and we ask everyone involved to adopt these principles. We are a diverse community. Sometimes this means we need to work harder to ensure we're creating an environment of trust and respect where all who come to participate feel comfortable and included.  

We value your participation and appreciate your help in realising this goal.  


### Participants in this ecosystem undertake to:

#### Be welcoming:
We welcome and support people of all backgrounds and identities. Remember that we’re a global ecosystem, so you might not be communicating in someone else’s primary language.

#### Be considerate:
Your work will be used by other people, and you in turn will depend on the work of others. Decisions you make will affect others; take consequences into account when making decisions. Acknowledge other people’s work.

#### Be respectful:
Not all of us will agree all the time. We might all experience some frustration now and then, but we cannot allow that frustration to turn into a personal attack. An ecosystem where people feel uncomfortable or threatened is not a productive one.

#### Be careful in the words that we choose:
Do not insult or put down other participants. Harassment and other exclusionary behaviours are not acceptable. Be kind to others.

#### Understand why we disagree:
Disagreements, both social and technical, happen all the time. It is important that we resolve disagreements constructively. The strength of our ecosystem comes from its diversity. Focus on helping to resolve issues and learning from mistakes.

### Innovation begins with inclusion.
We will not tolerate discrimination based on characteristics such as diversity in ethnicity, age, gender, gender identity or expression, culture, ability, language, national origin, political beliefs, profession, race, religion, sexual orientation, socioeconomic status, and technical ability.

### Everyone has the right to feel safe within the Fab Lab Wgtn ecosystem.
Our open ecosystem prioritises marginalised people’s safety over privileged people’s comfort. We will not act on complaints regarding:

* Reverse -isms, including ‘[reverse racism](https://thespinoff.co.nz/media/08-08-2018/a-friendly-reminder-that-reverse-racism-is-still-not-a-real-thing/)’ or ‘reverse sexism’  
* Reasonable communication of boundaries, such as “leave me alone,” “go away,” or “I’m not discussing this with you”  
* Refusal to explain or debate social justice concepts  
* Communicating in a ‘tone’ you don’t find congenial  
* Criticism of racist, sexist, or otherwise oppressive behaviour or assumptions  

We encourage everyone to participate in building an inclusive ecosystem. We seek to treat everyone as equitably as possible. When a participant has made a mistake, we expect them to take responsibility for it. If someone has been harmed or offended, it is our responsibility to listen respectfully, and do our best to right the wrong.

### Harassment includes, but is not limited to:
* Offensive or unwelcome comments related to any of the previously outlined protected characteristics
* Deliberate misgendering or misnaming. This includes persistently using a pronoun that does not correctly reflect a person’s gender identity.
* Deliberate intimidation
* Unwelcome sexual attention, including gratuitous or off-topic sexual images or behaviour
* Pattern of inappropriate social contact, such as requesting/assuming inappropriate levels of intimacy with others
* Continued one-on-one communication after requests to cease
* Deliberate “outing” of any aspect of a person’s identity without their consent except as necessary to protect others from intentional abuse


### Contact, Comments, Feedback
Inclusion is an ongoing process, guided by learning from people about their needs and experiences. We would be very happy to hear from you if you have any concerns, questions, or requests.  

If you experience any form of harassment or are made to feel unwelcome, [please let us know](/contact).  


This code focuses on principles of [Te Tiriti O Waitangi](https://www.schoolnews.co.nz/2016/11/te-tiriti-o-waitangi-living-the-values/) – in particular; partnership, equity and reciprocity –  
and uses information sourced from the [Open Code of Conduct](https://github.com/todogroup/opencodeofconduct) and [A More Perfect Union](http://www.pbs.org/ampu/crosscult.html#PATTERNS).
