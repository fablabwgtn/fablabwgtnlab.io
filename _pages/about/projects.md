---
layout: page
title:  "Ongoing Projects"
teaser: "Sustainability & Resilience"
categories:
    - about
header:
   image_fullwidth: homepage_ongoingprojects.jpg
   image_preview: homepage_ongoingprojects.jpg
show_meta: false
tag: involved
permalink: /about/projects/
---


Since 2013, we have been striving to create circular systems for our material use. Rather than bringing materials to the Lab and then sending away the waste, we aim to either create our own materials from local resources or intervene in, and divert materials from, the waste stream. 

At present we:

- run our 3D printers on NZ made PLA, a biodegradable plant-based plastic
- return all scrap 3D printing filament, filament bags and filament rolls to be recycled
- strive to use FSC and E0 plywoods in the laser cutter and on the large format milling machine
- are experimenting with making our own bio-plastics
- use corrugated cardboard in initial prototyping wherever possible
- are developing a system to re-use milk bottle tops and HDPE for machining

In the past we have:

- grown mushrooms on our poplar plywood waste
- made mycelium materials
- grown textile dye plants in our garden beds

We have identified three of the Sustainable Development Goals to focus on in everything we do:

{% include image.html src="sdg-goal-4.png,sdg-goal-15.png,sdg-goal-17.png" alt="UN Sustainable Goal 4, UN Sustainable Goal 15, UN Sustainable Goal 17" force_column_size="true" %}
