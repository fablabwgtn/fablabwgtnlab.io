---
layout: page
title:  "Camera Streaming"
teaser: "Watch what's going on in Fab Lab Wgtn"
categories:
    - about
header:
   image_fullwidth: homepage_ongoingprojects.jpg
   image_preview: homepage_ongoingprojects.jpg
show_meta: false
tag: involved
permalink: "/about/cameras/"
---

##### 3D Printer Streams
#### Ultimaker #1
<iframe src="https://fablabwgtn.co.nz/streams/29f6d48b-461f-4e00-8a83-817c898673f0.html" width="640" height="360" frameborder="no" scrolling="no" allowfullscreen="true"></iframe>
#### Ultimaker #2
<iframe src="https://fablabwgtn.co.nz/streams/d50db6df-c333-476d-87c8-f666dc4b7841.html" width="640" height="360" frameborder="no" scrolling="no" allowfullscreen="true"></iframe>
#### Ultimaker #3
<iframe src="https://fablabwgtn.co.nz/streams/b28ae4b3-d94d-4053-84f6-25441e93621e.html" width="640" height="360" frameborder="no" scrolling="no" allowfullscreen="true"></iframe>
