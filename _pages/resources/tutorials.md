---
layout: page
title:  "Tutorials"
## subheadline:  "Fab Seat"
teaser: ""
categories:
    - resources
header:
   image_fullwidth: homepage_hold.png
show_meta: false
permalink: /resources/tutorials/
---

The curated list of resources linked from this page are intended as a supplement to the experiences offered within Fab Lab Wgtn.  


## 2D software

Digital Embroidery: [inkstitch beginner tutorials](https://inkstitch.org/tutorials/resources/beginner-video-tutorials/#)

Knitting machine AYAB: [how to set up your image using Inkscape & Gimp](https://manual.ayab-knitting.com/pattern_image_creation/)

Gimp: [Gimp website](https://www.gimp.org/tutorials/),  [Tutorials by VScorpianC](https://www.youtube.com/playlist?list=PLC8D4E4A83B04B284)

Inkscape: [Inkscape website](https://inkscape.org/learn/tutorials/),  [Tutorials by VScorpianC](https://www.youtube.com/playlist?list=PLDD9D41273639EF59)

Illustrator: You can click the lightbulb icon in Illustrator, top-right next to the workspace dropdown menu where it says 'essentials', to see some useful tutorials.

[Adobe tutorials](https://helpx.adobe.com/nz/illustrator/tutorials.html),  [A game to help you get better at using the Illustrator pen tool](https://bezier.method.ac/)

CO2 Laser: [Large Laser template.ait](https://gitlab.fablabwgtn.co.nz/fab-lab-wgtn/machine-templates/raw/master/lasercutter/large-laser-template.ait?inline=false)

Krita: [tutorials](https://docs.krita.org/en/tutorials.html)

## Shopbot - setting up your files
2D paths: [Vcarve Pro for beginners](https://www.youtube.com/watch?v=qt0CPmxFH0s)     

3D paths: [Vcarve Pro 3d toolpaths](https://www.youtube.com/watch?v=hbL4XeESEpQ)  

## 3D Modelling and Parametrics

Fusion 360: [Course for absolute beginners](https://f360ap.autodesk.com/courses#getting-started-for-absolute-beginners)

Rhino: [Tutorials](http://www.rhino3d.com/tutorials)    

Grasshopper: [Tutorials](http://www.grasshopper3d.com/page/tutorials-1)  

Make Human: [MakeHuman Wiki](http://www.makehumancommunity.org/wiki/Main_Page),  [a 35 minute intro by VScorpianC](https://youtu.be/PeLB63owHFc)

## Electronics

KiCAD: [sparkfun beginners](https://learn.sparkfun.com/tutorials/beginners-guide-to-kicad),  [KiCAD website](https://www.kicad-pcb.org/help/tutorials/)  

Eagle: [Tutorial One](https://www.youtube.com/watch?v=1AXwjZoyNno),  [Tutorial Two](https://www.youtube.com/watch?v=CCTs0mNXY24)  

DRU for Eagle: [fabmodule.dru](/assets/downloads/fabmodule.dru)    

Datasheets: [Octopart](http://octopart.com/)

Trinket M0: [setting up your programming environment](https://learn.adafruit.com/adafruit-trinket-m0-circuitpython-arduino/arduino-ide-setup)   

Calculating resistance: [LED resistor calculator](https://www.allaboutcircuits.com/tools/led-resistor-calculator/)

### Awesome Lists
[Fab Academy's Awesome List](https://github.com/Academany/awesome-fabacademy)

[Fiore Basile's Awesome List](https://github.com/fibasile/awesome)

### Other useful links
Brother knitting machine: [casting on](https://www.youtube.com/watch?v=L6D5O0mhaAU),  [casting off](https://www.youtube.com/watch?v=u33wzjCnyxM)

Bioplastics recipes: [FabTextiles Margaret Dunne](https://issuu.com/nat_arc/docs/bioplastic_cook_book_3), [Food for Thought](http://www.daniellewilde.com/wp-content/uploads/2018/10/SDU-Design_FoodForThought_24June2018.pdf), [Materiom](https://materiom.org/)
