---
layout: page
title: "Welcome Tours"
permalink: /welcometours/
header: no
---

### What can I do at Fab Lab Wgtn?

Sit on the comfy couches, eat your lunch and meet others, work in our Fab Studio on our computers, make e-textiles, do 3D printing, laser cutting, vinyl cutting, knitting, sewing and embroidery, discuss your ideas with the Fab Lab Wgtn staff and others, prototype almost anything, buy materials and kits, learn about 3D modelling, open source software and hardware, browse our samples and much more... 


### How do I get involved?

It is essential to attend a Welcome tour before you engage with Fab Lab Wgtn. Once you have done this, you can come in anytime we’re open to see what’s happening and you can attend any of the introduction activities. All Fabbers who engaged prior to 2022 must attend a Refreshing Welcome tour to update themselves.

### Upcoming tours for Massey Staff & Students

Our **Welcome** tours take approximately 1 hour during which time we discuss the culture of the space, safety issues and look at some of the workflows and possibilities. Please wear sturdy, closed-toe footwear.

Our **Refreshing** tours are for people who have already done an introductory tour to update them about our changes. These take approximately 30 minutes.

**There are some technical difficulties with the registration button - force-refresh the page by pressing shift as you click the refresh button, and try again.** 

<iframe src="//eventbrite.co.nz/tickets-external?eid=113852209022&amp;ref=etckt" frameborder="0" marginwidth="5" marginheight="5" scrolling="auto" width="100%" height="1000"></iframe>
