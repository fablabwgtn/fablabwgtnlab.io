#!/bin/bash
gem install bundler -v 2.4.7

echo "Install gems"

bundle config path vendor

bundle install --full-index



echo "Installing bootstrap, jquery and popper."

# Bootstrap scss files
if [ ! -d "_sass" ]
then
  mkdir _sass
fi
cp vendor/ruby/2.7.0/gems/bootstrap-4.5.0/assets/stylesheets/* _sass/ -r

# Bootstrap js files
if [ ! -d "assets/js/" ]
then
  mkdir assets/js/
fi
cp vendor/ruby/2.7.0/gems/bootstrap-4.5.0/assets/javascripts/bootstrap.min.js assets/js/

# Jquery js files
cp vendor/ruby/2.7.0/gems/jquery-rails-4.4.0/vendor/assets/javascripts/jquery.min.js assets/js/

# Popper.js files
cp vendor/ruby/2.7.0/gems/popper_js-1.16.0/assets/javascripts/popper.js assets/js/
