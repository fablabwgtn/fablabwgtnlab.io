<?php
if(isset($_POST['submit']) && !empty($_POST['submit'])):
    if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])):
        //your site secret key, 'keep it secret, keep it safe' ish
        $secret = '6Le5aw8UAAAAAETG-ajGhF4CNivO7UdWbACmD7Xy';
        //get verify response data
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success):
            //contact form submission code
            $name = !empty($_POST['name'])?$_POST['name']:'';
            $email = !empty($_POST['email'])?$_POST['email']:'';
            $message = !empty($_POST['message'])?$_POST['message']:'';

            $to = 'fablabwgtn@massey.ac.nz';
            $subject = 'Website Contact Form';
            $htmlContent = "
                <h1>Contact request details</h1>
                <p><b>Name: </b>".$name."</p>
                <p><b>Email: </b>".$email."</p>
                <p><b>Message: </b>".$message."</p>
            ";
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers
            $headers .= 'From: Fab Lab Wgtn Contact Form <noreply@fablabwgtn.co.nz>' . "\r\n";
            $headers .= 'Reply-To: '. $email . "\r\n";
            //send email
            @mail($to,$subject,$htmlContent,$headers);

            $succMsg = 'Your contact request have submitted successfully.';
        else:
            $errMsg = 'Robot verification failed, please try again.';
        endif;
    else:
        $errMsg = 'Please click on the reCAPTCHA box.';
    endif;
else:
    $errMsg = '';
    $succMsg = '';
endif;
if($succMsg != null):
    header("Location: /contact/success/");
endif;
if($errMsg != null):
    header("Location: /contact/retry/");
endif;
echo $errMsg;
echo $succMsg;
?>
