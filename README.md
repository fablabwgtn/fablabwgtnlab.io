# Fab Lab Wgtn Website

#### Local build instructions
1. Install Ruby:  
  * MacOS & Linux: [RVM](https://rvm.io/)  
  * Windows: [RubyInstaller](https://rubyinstaller.org/)
2. Instal gems:  
  * ```./install.sh```
3. Run jekyll:  
  * ```jekyll serve --livereload```
4. Open ```localhost:4000``` in your browser



# Building a new version of the website with docker-compose

1. Clone this repo
2. Find the relevant docker-compose in the FabLab Sharepoint (Internal > 5. Projects & Backend > Backend > wel-fablab3-docker > website-autobuild)
3. Run the docker compose (```docker compose up```), if running on ```wel-fablab3``` then no changes need to be made. If building locally then pay attention to the volume mounts so that the built website gets exported in the right location!

#### Notes

The docker-compose uses the dockerfile in the 'jekyll' directory, which automatically pulls this repo. Its basically just a ruby container with this repo + jekyll build tings.

On wel-fablab3 the compose file can be found in the home dir of the root user in the 'docker' directory.



Daniel = 👑
